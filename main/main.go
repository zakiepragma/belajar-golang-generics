package main

import "fmt"

// non-generic
func sumOfInt(input []int) int {
	inc := 0
	for _, val := range input {
		inc += val
	}
	return inc
}

// generic
func sumOf[T int64](input []T) T {
	var inc T
	for _, val := range input {
		inc += val
	}
	return inc
}
func main() {
	//output non-generic
	fmt.Printf("%d", sumOfInt([]int{0, 1, 2, 3, 4, 5}))
	//output generic
	fmt.Printf("%d", sumOf([]int64{0, 1, 2, 3, 4, 5}))
}
