package belajargolanggenerics

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func Length[T any](param T) T {
	fmt.Println(param)
	return param
}

func TestLength(t *testing.T) {
	var result string = Length[string]("al muzani")
	assert.Equal(t, "al muzani", result)

	var result2 int = Length[int](2023)
	assert.Equal(t, 2023, result2)
}
